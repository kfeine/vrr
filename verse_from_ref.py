#!/usr/bin/python

import sys
import re
import requests
import json

version = "ls1910"
for text in sys.argv[1:]:
    ref_match = re.match(r"(?P<book_ref>[\w ]+)[ \.]+(?P<chapter_ref>\d+)[ \.:](?P<verse_start_ref>\d+)(-(?P<verse_end_ref>\d+))?( +(?P<version>\w+))?", text)
    if ref_match is None:
        exit(0)
    else:
        book_ref = ref_match.group("book_ref")
        chapter_ref = ref_match.group("chapter_ref")
        verse_start_ref = ref_match.group("verse_start_ref")
        verse_end_ref = ref_match.group("verse_end_ref")
        verse_ref = verse_start_ref
        if verse_end_ref is not None:
            verse_ref += "-" + verse_end_ref

        if re.match("(gen.*)|(gn)", book_ref, re.IGNORECASE):
            book = "genesis"
        elif re.match("ex.*", book_ref, re.IGNORECASE):
            book = "exodus"
        elif re.match("(lev.*)|(lv)", book_ref, re.IGNORECASE):
            book = "leviticus"
        elif re.match("(nu.*)|(nm)|(nb)", book_ref, re.IGNORECASE):
            book = "numbers"
        elif re.match("(deut.*)|(det.*)|(dt)", book_ref, re.IGNORECASE):
            book = "deuteronomy"
        elif re.match("(jo.*)", book_ref, re.IGNORECASE):
            book = "joshua"
        elif re.match("(ju.*)|(jd.*)", book_ref, re.IGNORECASE):
            book = "judges"
        elif re.match("(ru.*)|(rt)", book_ref, re.IGNORECASE):
            book = "ruth"
        elif re.match("(1 *s.*)", book_ref, re.IGNORECASE):
            book = "1 samuel"
        elif re.match("(2 *s.*)", book_ref, re.IGNORECASE):
            book = "2 samuel"
        elif re.match("(1 *r.*)|(1 *k.*)", book_ref, re.IGNORECASE):
            book = "1 kings"
        elif re.match("(2 *r.*)|(2 *k.*)", book_ref, re.IGNORECASE):
            book = "2 kings"
        elif re.match("(1 *c.*)", book_ref, re.IGNORECASE):
            book = "1 chronicles"
        elif re.match("(2 *c.*)", book_ref, re.IGNORECASE):
            book = "2 chronicles"
        elif re.match("(ezr.*)|(esd.*)", book_ref, re.IGNORECASE):
            book = "ezra"
        elif re.match("(ne.*)", book_ref, re.IGNORECASE):
            book = "nehemiah"
        elif re.match("(est.*)", book_ref, re.IGNORECASE):
            book = "esther"
        elif re.match("(job)|(jb)", book_ref, re.IGNORECASE):
            book = "job"
        elif re.match("(ps.*)", book_ref, re.IGNORECASE):
            book = "psalms"
        elif re.match("(pr.*)", book_ref, re.IGNORECASE):
            book = "proverbs"
        elif re.match("(ec.*)", book_ref, re.IGNORECASE):
            book = "ecclesiastes"
        elif re.match("(ca.*)|(ct.*)|(son.*)|(sg.*)", book_ref, re.IGNORECASE):
            book = "song of songs"
        elif re.match("(es.*)|(is.*)", book_ref, re.IGNORECASE):
            book = "isaiah"
        elif re.match("(jer*)|(jr)", book_ref, re.IGNORECASE):
            book = "jeremiah"
        elif re.match("(lam.*)|(lm)", book_ref, re.IGNORECASE):
            book = "lamentations"
        elif re.match("(ez.*)", book_ref, re.IGNORECASE):
            book = "ezekial"
        elif re.match("(dan.*)|(dn)", book_ref, re.IGNORECASE):
            book = "daniel"
        elif re.match("(hos.*)|(os.*)", book_ref, re.IGNORECASE):
            book = "hosea"
        elif re.match("(jo.*)|(jl)", book_ref, re.IGNORECASE):
            book = "joel"
        elif re.match("(am.*)", book_ref, re.IGNORECASE):
            book = "amos"
        elif re.match("(ob.*)|(ab.*)", book_ref, re.IGNORECASE):
            book = "obadiah"
        elif re.match("(jon.*)", book_ref, re.IGNORECASE):
            book = "jonah"
        elif re.match("(mic.*)|(mi)", book_ref, re.IGNORECASE):
            book = "micah"
        elif re.match("(nah.*)|(nh)", book_ref, re.IGNORECASE):
            book = "nahum"
        elif re.match("(ha.*)", book_ref, re.IGNORECASE):
            book = "habakkuk"
        elif re.match("(ze.*)|(so.*)", book_ref, re.IGNORECASE):
            book = "zephaniah"
        elif re.match("(ha.*)|(hg)|(ag.*)", book_ref, re.IGNORECASE):
            book = "haggai"
        elif re.match("(ze.*)|(za.*)", book_ref, re.IGNORECASE):
            book = "zechariah"
        elif re.match("(mal.*)|(ml)", book_ref, re.IGNORECASE):
            book = "malachi"
        elif re.match("(mat.*)|(mt.*)", book_ref, re.IGNORECASE):
            book = "matthew"
        elif re.match("(ma.*)|(mk)|(mc)", book_ref, re.IGNORECASE):
            book = "mark"
        elif re.match("(lu.*)|(lk.*)|(lc)", book_ref, re.IGNORECASE):
            book = "luke"
        elif re.match("(joh.*)|(jn)|(jea.*)", book_ref, re.IGNORECASE):
            book = "john"
        elif re.match("(ac.*)", book_ref, re.IGNORECASE):
            book = "acts"
        elif re.match("(ro.*)|(rm)", book_ref, re.IGNORECASE):
            book = "romans"
        elif re.match("(1 *co.*)", book_ref, re.IGNORECASE):
            book = "1 corinthians"
        elif re.match("(2 *co.*)", book_ref, re.IGNORECASE):
            book = "2 corinthians"
        elif re.match("(ga.*)", book_ref, re.IGNORECASE):
            book = "galatians"
        elif re.match("(ep.*)", book_ref, re.IGNORECASE):
            book = "ephesians"
        elif re.match("(ph.*)", book_ref, re.IGNORECASE):
            book = "philippians"
        elif re.match("(co.*)", book_ref, re.IGNORECASE):
            book = "colossians"
        elif re.match("(1 *th.*)", book_ref, re.IGNORECASE):
            book = "1 thessalonians"
        elif re.match("(2 *th.*)", book_ref, re.IGNORECASE):
            book = "2 thessalonians"
        elif re.match("(1 *ti.*)", book_ref, re.IGNORECASE):
            book = "1 timothy"
        elif re.match("(2 *ti.*)", book_ref, re.IGNORECASE):
            book = "2 timothy"
        elif re.match("(ti.*)", book_ref, re.IGNORECASE):
            book = "titus"
        elif re.match("(phi.*)|(phl.*)", book_ref, re.IGNORECASE):
            book = "philemon"
        elif re.match("(he.*)|(hb)", book_ref, re.IGNORECASE):
            book = "hebrews"
        elif re.match("(jam.*)|(ja.*)|(jq.*)|(jc.*)", book_ref, re.IGNORECASE):
            book = "james"
        elif re.match("(1 *p.*)", book_ref, re.IGNORECASE):
            book = "1 peter"
        elif re.match("(2 *p.*)", book_ref, re.IGNORECASE):
            book = "2 peter"
        elif re.match("(1 *j.*)", book_ref, re.IGNORECASE):
            book = "1 john"
        elif re.match("(2 *j.*)", book_ref, re.IGNORECASE):
            book = "2 john"
        elif re.match("(3 *j.*)", book_ref, re.IGNORECASE):
            book = "3 john"
        elif re.match("(ju.*)", book_ref, re.IGNORECASE):
            book = "jude"
        elif re.match("(rev.*)|(ap.*)", book_ref, re.IGNORECASE):
            book = "revelation"
        else:
            exit(0)

        http_resp = requests.get("https://getbible.net/json?scrip={} {}:{}&version={}".format(book, chapter_ref, verse_ref, version))
        if http_resp.status_code != 200:
            # This means something went wrong.
            raise ApiError(http_resp.status_code)
        http_resp_content = json.loads(http_resp.text[1:-2])["book"][0]
        verses = ""
        for i in http_resp_content["chapter"]:
            verses += http_resp_content["chapter"][i]["verse"][0:-2]
            verses += " "
        print(u"{} ({} {}.{} {})".format(verses, book_ref, chapter_ref, verse_ref, version).encode("utf-8"))
